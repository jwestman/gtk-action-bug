/* gtk_action_bug-window.c
 *
 * Copyright 2020 James Westman
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written
 * authorization.
 */

#include "gtk_action_bug-config.h"
#include "gtk_action_bug-window.h"

struct _GtkActionBugWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkFrame            *frame1;
  GtkFrame            *frame2;
  GtkFrame            *frame3;
};

G_DEFINE_TYPE (GtkActionBugWindow, gtk_action_bug_window, GTK_TYPE_APPLICATION_WINDOW)

static void
on_reparent (GSimpleAction   *action,
             GVariant        *parameter,
             gpointer         user_data)
{
  GtkActionBugWindow *self = GTK_ACTION_BUG_WINDOW (user_data);


  if (gtk_widget_get_parent (GTK_WIDGET (self->frame3)) == GTK_WIDGET (self->frame1)) {
    gtk_container_remove (GTK_CONTAINER (self->frame1), GTK_WIDGET (self->frame3));
    gtk_container_add (GTK_CONTAINER (self->frame2), GTK_WIDGET (self->frame3));
  } else {
    gtk_container_remove (GTK_CONTAINER (self->frame2), GTK_WIDGET (self->frame3));
    gtk_container_add (GTK_CONTAINER (self->frame1), GTK_WIDGET (self->frame3));
  }
}

static void
gtk_action_bug_window_class_init (GtkActionBugWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/App/gtk_action_bug-window.ui");
  gtk_widget_class_bind_template_child (widget_class, GtkActionBugWindow, frame1);
  gtk_widget_class_bind_template_child (widget_class, GtkActionBugWindow, frame2);
  gtk_widget_class_bind_template_child (widget_class, GtkActionBugWindow, frame3);
}

static void
on_action_3 (GSimpleAction   *action,
             GVariant        *parameter,
             gpointer         user_data)
{
  printf("Action 3!\n");
}

static void
on_action_1 (GSimpleAction   *action,
             GVariant        *parameter,
             gpointer         user_data)
{
  printf("Action 1!\n");
}

static void
gtk_action_bug_window_init (GtkActionBugWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  const GActionEntry entries_win[] = { { "reparent", on_reparent } };
  GSimpleActionGroup *group_win = g_simple_action_group_new ();
  g_action_map_add_action_entries (G_ACTION_MAP (group_win), entries_win, G_N_ELEMENTS(entries_win), self);
  gtk_widget_insert_action_group (GTK_WIDGET (self), "win", G_ACTION_GROUP (group_win));

  /* Comment this out */
  const GActionEntry entries_frm_1[] = { { "action", on_action_1 } };
  GSimpleActionGroup *group_frm_1 = g_simple_action_group_new ();
  g_action_map_add_action_entries (G_ACTION_MAP (group_frm_1), entries_frm_1, G_N_ELEMENTS(entries_frm_1), self);
  gtk_widget_insert_action_group (GTK_WIDGET (self->frame1), "frm", G_ACTION_GROUP (group_frm_1));

  const GActionEntry entries_frm_3[] = { { "action", on_action_3 } };
  GSimpleActionGroup *group_frm_3 = g_simple_action_group_new ();
  g_action_map_add_action_entries (G_ACTION_MAP (group_frm_3), entries_frm_3, G_N_ELEMENTS(entries_frm_3), self);
  gtk_widget_insert_action_group (GTK_WIDGET (self->frame3), "frm", G_ACTION_GROUP (group_frm_3));
}
